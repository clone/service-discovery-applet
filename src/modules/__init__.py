# -*- coding: UTF-8 -*-
# -*- python -*-
# Copyright (C) 2005 by Sebastien Estienne
#
# This file may be distributed and/or modified under the terms of
# the GNU General Public License version 2 as published by
# the Free Software Foundation.
# This file is distributed without any warranty; without even the implied
# warranty of merchantability or fitness for a particular purpose.
# See "COPYING" in the source distribution for more information.
#
# $id$
