# -*- coding: utf-8 -*-
# -*- python -*-
# Copyright (C) 2005 by Sebastien Estienne
#
# This file may be distributed and/or modified under the terms of
# the GNU General Public License version 2 as published by
# the Free Software Foundation.
# This file is distributed without any warranty; without even the implied
# warranty of merchantability or fitness for a particular purpose.
# See "COPYING" in the source distribution for more information.
#
# $id$
#

import pygtk
import gtk

pygtk.require('2.0')

def error_msg(msg):
    message(msg, "error")

def message(msg, type="info"):
    msg_type = eval("gtk.MESSAGE_%s" % type.upper())
    d = gtk.MessageDialog(parent=None, flags=gtk.DIALOG_MODAL,
                          type=msg_type, buttons=gtk.BUTTONS_OK)
    d.set_markup(msg)
    d.show_all()
    d.run()
    d.destroy()

def pair_to_dict(l):
    res = dict()
    for el in l:
        if "=" not in el:
            res[el]=''
        else:
            tmp = el.split('=',1)
            if len(tmp[0]) > 0:
                res[tmp[0]] = tmp[1]
    return res
                                                                

def build_url(uri = "http", hostname="127.0.0.1", port = None, path = None, username = None, password = None):
    if path and path != None:
        if path[0] == "/":
            path = path[1:]
    else:
        path = ""
        
    if username:
        if password:
            username="%s:%s@" % (username,password)
        else:
            username="%s@" % (username)
    else:
        username=""
    if port and port != None:
        hostname="%s:%i" % (hostname,port)
    return "%s://%s%s/%s" % (uri,username,hostname,path)

def get_txt_value(txts, txt):
    if txts.has_key(txt):
        return txts[txt]
    else:
        return None
